package app.tutorial.taman.realmandretrofitdemowithopenweather;

/**
 * Created by Xerric on 12/13/2016.
 */

public class Constants {

    public static class URL {
        public static final String BASE_URL = "http://api.openweathermap.org/data/2.5/";
        public static final String WEATHER_ICON_BASE_URL="http://openweathermap.org/img/w/";
    }
}
