package app.tutorial.taman.realmandretrofitdemowithopenweather.retrofitapi;

import app.tutorial.taman.realmandretrofitdemowithopenweather.helper.OpenWeatherApiResponse;

/**
 * Created by Xerric on 12/12/2016.
 */

public interface AllServerCallListner {

    public interface WeatherDataDowloadListner {
        void setDownloadSuccess(OpenWeatherApiResponse openWeatherApiResponse);

        void setDownloadFailure(String message);
    }

}
