package app.tutorial.taman.realmandretrofitdemowithopenweather.retrofitapi;

import app.tutorial.taman.realmandretrofitdemowithopenweather.helper.OpenWeatherApiResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Xerric on 12/12/2016.
 */

public class AllServerCallMethods {

    public AllServerCallListner.WeatherDataDowloadListner weatherDataDowloadListner;

    public AllServerCallMethods() {

    }

    //to get weather data
    public void downloadWeatherData(String url) {
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<OpenWeatherApiResponse> call = apiInterface.getWeatherData(url);
        call.enqueue(new Callback<OpenWeatherApiResponse>() {
            @Override
            public void onResponse(Call<OpenWeatherApiResponse> call, Response<OpenWeatherApiResponse> response) {
                if (response.code() == 200) {
                    weatherDataDowloadListner.setDownloadSuccess(response.body());
                } else {
                    weatherDataDowloadListner.setDownloadFailure("Response code : " + response.code());
                }
            }

            @Override
            public void onFailure(Call<OpenWeatherApiResponse> call, Throwable t) {
                // called in no internet condition
                weatherDataDowloadListner.setDownloadFailure(t.getLocalizedMessage());
            }
        });
    }

}
