package app.tutorial.taman.realmandretrofitdemowithopenweather.retrofitapi;


import app.tutorial.taman.realmandretrofitdemowithopenweather.helper.OpenWeatherApiResponse;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Url;


/**
 * Created by taman on 8/8/2015.
 */
public interface ApiInterface {

    @GET
    Call<OpenWeatherApiResponse> getWeatherData(@Url String url);
}

