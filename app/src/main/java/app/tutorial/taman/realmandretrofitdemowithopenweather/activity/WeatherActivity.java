package app.tutorial.taman.realmandretrofitdemowithopenweather.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

import app.tutorial.taman.realmandretrofitdemowithopenweather.Constants;
import app.tutorial.taman.realmandretrofitdemowithopenweather.R;
import app.tutorial.taman.realmandretrofitdemowithopenweather.helper.OpenWeatherApiResponse;
import app.tutorial.taman.realmandretrofitdemowithopenweather.realm.RealmController;
import app.tutorial.taman.realmandretrofitdemowithopenweather.retrofitapi.AllServerCallListner;
import app.tutorial.taman.realmandretrofitdemowithopenweather.retrofitapi.AllServerCallMethods;
import butterknife.Bind;
import butterknife.ButterKnife;
import io.realm.Realm;
import io.realm.RealmChangeListener;
import io.realm.RealmResults;

public class WeatherActivity extends AppCompatActivity implements AllServerCallListner.WeatherDataDowloadListner {

    @Bind(R.id.city_name_et)
    EditText city_name_et;

    @Bind(R.id.btn_get_weather)
    Button btn_get_weather;

    @Bind(R.id.place_name)
    TextView place_name;

    @Bind(R.id.temperature)
    TextView temperature;

    @Bind(R.id.description)
    TextView description;

    @Bind(R.id.sunset)
    TextView sunset;

    @Bind(R.id.sunrise)
    TextView sunrise;

    @Bind(R.id.weather_icon)
    ImageView weather_icon;


    private Realm realm;

    private RealmChangeListener realmChangeListener;

    OpenWeatherApiResponse openWeatherApiResponse;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_weather);

        ButterKnife.bind(this);

        openWeatherApiResponse = new OpenWeatherApiResponse();
        //get realm instance
        this.realm = RealmController.with(this).getRealm();

        realmChangeListener = new RealmChangeListener() {
            @Override
            public void onChange(Object element) {
                updateUI(openWeatherApiResponse);
            }
        };
        this.realm.addChangeListener(realmChangeListener);


        final AllServerCallMethods allServerCallMethods = new AllServerCallMethods();
        allServerCallMethods.weatherDataDowloadListner = this;

        btn_get_weather.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String cityname = city_name_et.getText().toString();
                allServerCallMethods.downloadWeatherData(Constants.URL.BASE_URL + "weather?q=" + cityname + ",np&units=metric&type=accurate&appid=" + getAPIKey());
            }
        });

        allServerCallMethods.downloadWeatherData("Load Previous data");

    }

    private void updateUI(OpenWeatherApiResponse openWeatherApiResponse) {
        try {
            temperature.setText(Math.round(Float.parseFloat(openWeatherApiResponse.getMain().getTemp() + "")) + " °C");
            place_name.setText(openWeatherApiResponse.getName());
            description.setText(openWeatherApiResponse.getWeather().get(0).getDescription());

            long sunrise_unix = openWeatherApiResponse.getSys().getSunrise() * (long) 1000;
            Date sunrise_date = new Date(sunrise_unix);
            SimpleDateFormat format = new SimpleDateFormat("EEE HH:mm");
            format.setTimeZone(TimeZone.getTimeZone("GMT+05:45")); // according to mine is +05:45
            sunrise.setText("Sunrise : " + format.format(sunrise_date));

            long sunset_unix = openWeatherApiResponse.getSys().getSunset() * (long) 1000;
            Date sunset_date = new Date(sunset_unix);
            sunset.setText("Sunset : " + format.format(sunset_date));

            Picasso.with(WeatherActivity.this).load(Constants.URL.WEATHER_ICON_BASE_URL + openWeatherApiResponse.getWeather().get(0).getIcon() + ".png").into(weather_icon);
        } catch (Exception e) {
            Toast.makeText(WeatherActivity.this, e.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void setDownloadSuccess(OpenWeatherApiResponse openWeatherApiResponse) {
        Toast.makeText(WeatherActivity.this, openWeatherApiResponse.getName(), Toast.LENGTH_SHORT).show();
        this.openWeatherApiResponse = openWeatherApiResponse;
        //saving to realm
        realm.beginTransaction();
        realm.copyToRealm(openWeatherApiResponse);
        realm.commitTransaction();
    }

    @Override
    public void setDownloadFailure(String message) {
        Toast.makeText(WeatherActivity.this, message, Toast.LENGTH_SHORT).show();
        RealmResults<OpenWeatherApiResponse> realmResults = RealmController.with(WeatherActivity.this).getData();
        if (realmResults.size() != 0) {
            openWeatherApiResponse = realmResults.get(realmResults.size() - 1);
            updateUI(openWeatherApiResponse);
        } else {
            Toast.makeText(WeatherActivity.this, "No previous data in database", Toast.LENGTH_SHORT).show();
        }
    }

    private String getAPIKey() {
        return getResources().getString(R.string.api_open_weather);
    }

    @Override
    protected void onDestroy() {
        ButterKnife.unbind(this);
        super.onDestroy();
    }
}
