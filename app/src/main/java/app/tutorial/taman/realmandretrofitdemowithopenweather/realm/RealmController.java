package app.tutorial.taman.realmandretrofitdemowithopenweather.realm;

import android.app.Activity;
import android.app.Application;
import android.support.v4.app.Fragment;

import app.tutorial.taman.realmandretrofitdemowithopenweather.helper.OpenWeatherApiResponse;
import io.realm.Realm;
import io.realm.RealmResults;

/**
 * Created by Xerric on 12/13/2016.
 */

public class RealmController {
    private static RealmController instance;
    private final Realm realm;

    public RealmController(Application application) {
        realm = Realm.getDefaultInstance();
    }

    public static RealmController with(Fragment fragment) {

        if (instance == null) {
            instance = new RealmController(fragment.getActivity().getApplication());
        }
        return instance;
    }

    public static RealmController with(Activity activity) {

        if (instance == null) {
            instance = new RealmController(activity.getApplication());
        }
        return instance;
    }

    public static RealmController with(Application application) {

        if (instance == null) {
            instance = new RealmController(application);
        }
        return instance;
    }

    public static RealmController getInstance() {

        return instance;
    }

    public Realm getRealm() {

        return realm;
    }

    //Refresh the realm istance
    public void refresh() {

        realm.setAutoRefresh(true);
    }

    //find all objects in the Data.class
    public RealmResults<OpenWeatherApiResponse> getData() {

        return realm.where(OpenWeatherApiResponse.class).findAll();
    }

    public void clearAllProducts() {
        realm.beginTransaction();
        realm.delete(OpenWeatherApiResponse.class);
        realm.commitTransaction();
    }
}
